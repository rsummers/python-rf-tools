#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 18:37:28 2019

@author: rsummers
"""

import numbers

import numpy as np
import skrf as rf
from scipy import constants

from engineering_notation import EngNumber

def coupling_amatrix(m, Omega):
    l = len(m)
    N = l-2
    
    r = np.zeros([N+2, N+2])
    r[0, 0] = 1
    r[N+1, N+1] = 1
    
    w = np.identity(N+2)
    w = w - r
    
    a = r + 1j*Omega*w + 1j*m
    return a

def m2s(m, freq, f0, fbw, z0=50):
    """
    Returns the S-parameters of a coupling matrix over a given frequency
    band (specified as a skrf Frequency object) and given
    center frequency (in Hz), fractional bandwidth, and characteristic
    impedance.
    """
    
    f = freq.f
    Omega = (1/fbw)*(f/f0 - f0/f)
    
    s11 = np.zeros(freq.npoints, dtype=np.complex_)
    s12 = np.zeros(freq.npoints, dtype=np.complex_)
    s21 = np.zeros(freq.npoints, dtype=np.complex_)
    s22 = np.zeros(freq.npoints, dtype=np.complex_)
    
    for i in range(freq.npoints):
        a = coupling_amatrix(m, Omega[i])
        ainv = np.linalg.inv(a)
        s11[i] = 1 - 2*ainv[0, 0]
        s12[i] = 2*ainv[0, -1]
        s21[i] = 2*ainv[-1, 0]
        s22[i] = 1 - 2*ainv[-1, -1]
    s = np.moveaxis(np.array([[s11, s12], [s21, s22]]), -1, 0)
    net = rf.network.Network(f=f, s=s, z0=z0*np.ones(freq.npoints))
    return net

def m2design_coefficients(m, fbw):
    """
    Returns a matrix with elements on the edge being external quality factors
    (Q_{e,Si} or Q_{e,Li}) and internal elements being coupling coefficients
    (k_{mn}) corresponding to the elements of the given (N+2)x(N+2) coupling
    matrix.
    """
    coeffs = np.zeros(m.shape)
    N = len(m) - 2
    for i, j in np.ndindex(m.shape):
        if (i == 0) or (j == 0) or (i == N+1) or (j == N+1):
            coeffs[i, j] = 1/(m[i, j]**2 * fbw)
        else:
            coeffs[i, j] = fbw*m[i, j]
    return coeffs

def m2inline_design_coefficients(m, fbw):
    N = len(m) - 2
    coeffs = m2design_coefficients(m, fbw)
    clist = []
    clist.append(('Q_e,S1', coeffs[0, 1]))
    name = 'Q_e,' + str(N) + 'L'
    clist.append((name, coeffs[-2, -1]))
    for i in range(N-1):
        name = 'k_' + str(i+1) + str(i+2)
        clist.append((name, coeffs[i+1, i+2]))
    return clist