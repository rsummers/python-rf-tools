# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 15:04:36 2019

@author: Randall Summers
"""

import numbers

import numpy as np
import skrf as rf
from scipy import constants

from engineering_notation import EngNumber

pi = constants.pi

#from scipy import signal
#from sympy import *
#
#init_printing()
#s = symbols('s')
#
#z, p, k = signal.cheb1ap(5, 0.05)
#b, a = signal.zpk2tf(z, p, k)
#
#num = Poly.from_list(b, gens=s)
#den = Poly.from_list(a, gens=s)
#
#tf = num/den

def gvalues(ftype="butt", N=3, rp=0.1, rl=None):
    """
    Returns the lowpass prototype coefficients for the given filter.
    ftype can be either "butt" or "cheb1" for a Butterworth or
    Chebyshev Type 1 filter respectively.
    N is the order of the filter.

    rp is the filter passband ripple of a Chebyshev Type 1 filter, in dB.
    This parameter does not affect the gvalues if a Butterworth filter is
    selected.

    The filter ripple can alternatively be specified using rl, which
    represents the (maximum ?) passband return loss, in dB.
    """

    gvals = np.zeros(N+1)

    if ftype == "butt":
        for r in range(1, N+1):
            gvals[r-1] = 2*np.sin((2*r-1)*pi/(2*N))
        gvals[N] = 1.0

    elif ftype == "cheb1":
        if not(rl is None):
            gammamag = 10**(-1*rl/20)
            rp = -10*np.log10(1-gammamag**2)
        epsilon = np.sqrt(10**(0.1*rp)-1)
        eta = np.sinh(1/N*np.arcsinh(1/epsilon))
        gvals[0] = 2/eta*np.sin(pi/(2*N))
        for r in range(1, N):
            a = 4*np.sin((2*r-1)*pi/(2*N))
            b = np.sin((2*r+1)*pi/(2*N))
            c = eta**2+np.sin(r*pi/N)**2
            gvals[r] = a*b/(c*gvals[r-1])
        if (N % 2) == 1:
            gvals[N] = 1.0
        else:
            gvals[N] = (epsilon+np.sqrt(1+epsilon**2))**2

    return gvals

def filter_transform_ind(L, ttype="LP", w0=1, r0=1, fbw=1):
    if ttype == "LP":
        return ("ind", EngNumber(float(r0*L/w0)))
    if ttype == "HP":
        return ("cap", EngNumber(float(1/(r0*w0*L))))
    if ttype == "BP":
        return ("series", "ind", EngNumber(float(r0*L/(w0*fbw))), "cap", EngNumber(float(fbw/(r0*w0*L))))
    if ttype == "BS":
        return ("parallel", "ind", EngNumber(float(r0*L*fbw/w0)), "cap", EngNumber(float(1/(r0*w0*L*fbw))))

def filter_transform_cap(C, ttype="LP", w0=1, r0=1, fbw=1):
    if ttype == "LP":
        return ("cap", EngNumber(float(C/(r0*w0))))
    if ttype == "HP":
        return ("ind", EngNumber(float(r0/(w0*C))))
    if ttype == "BP":
        return ("parallel", "ind", EngNumber(float(r0*fbw/(w0*C))), "cap", EngNumber(float(C/(r0*w0*fbw))))
    if ttype == "BS":
        return ("series", "ind", EngNumber(float(r0/(w0*C*fbw))), "cap", EngNumber(float(C*fbw/(r0*w0))))

def filter_il(gvals, fbw, Q):
    """
    Returns the approximate passband insertion loss of a filter with given
    gvalues, fractional bandwidth, and finite resonator unloaded Q. The Q
    can be specified as a constant for all resonating elements or as
    an array-like with the nth Q corresponding to the nth gvalue.
    """
    N = len(gvals) - 1
    gvals = gvals[:N]
    if isinstance(Q, numbers.Number):
        Qi = np.ones(N)*Q
    return 4.343/fbw*sum(gvals/Qi)

def gvalues2m(gvals):
    """
    Returns the general (N+2)x(N+2) coupling matrix for a filter with the
    given g-values.
    """
    N = len(gvals) - 1
    m = np.zeros([N+2, N+2])
    g0 = 1

    m[0, 1] = 1/np.sqrt(g0*gvals[0])
    m[1, 0] = 1/np.sqrt(g0*gvals[0])
    for i in range(1,N):
        m[i,i+1] = 1/np.sqrt(gvals[i-1]*gvals[i])
        m[i+1,i] = 1/np.sqrt(gvals[i-1]*gvals[i])
    m[N, N+1] = 1/np.sqrt(gvals[N-1]*gvals[N])
    m[N+1, N] = 1/np.sqrt(gvals[N-1]*gvals[N])
    return m

def asymmetric_interdigital(gvals, fbw, y1=1/50):
    N = len(gvals)
    theta = pi/2*(1-fbw/2)
    y = y1/(np.tan(theta))
    Ji = np.zeros(gvals.shape)
    yi = np.zeros(gvals.shape)
    for i in range(N-1):
        Ji[i] = y/np.sqrt(gvals[i]*gvals[i+1])
        yi[i] = Ji[i]*np.sin(theta)
    z0e = np.zeros(gvals.shape)
    z0o = np.zeros(gvals.shape)

    z0e[0] = 1/(y1 - yi[0])
    z0o[0] = 1/(y1 + yi[0])

    z0e[-1] = 1/(y1 - yi[-1])
    z0o[-1] = 1/(y1 + yi[-1])

    for i in range(1, N-1):
        z0e[i] = 1/(2*y1 - 1/z0e[i-1] - yi[i] - yi[i-1])
        z0o[i] = 1/(2*yi[i] + 1/z0e[i])

    return z0e, z0o
