# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 10:13:10 2019

@author: Randall Summers
"""

import numpy as np
from scipy import constants
import itertools
from engineering_notation import EngNumber
from tabulate import tabulate

pi = np.pi
ep_0 = constants.epsilon_0
mu_0 = constants.mu_0
c_0 = constants.speed_of_light

class SeriesRLC:
    
    def __init__(self, R=0, L=1, C=1, R_L=0):
        self.R = R
        self.L = L
        self.C = C
        self.R_L = R_L
    
    @property
    def w_0(self):
        return 1/np.sqrt(self.L*self.C)
    
    @property
    def f_0(self):
        return self.w_0/(2*pi)
    
    @property
    def Q_0(self):
        return self.w_0*self.L/self.R
    
    @property
    def Q_e(self):
        return self.w_0*self.L/self.R_L
    
    @property
    def Q(self):
        return (1/self.Q_e + 1/self.Q_0)**-1

class ParallelRLC:
    
    def __init__(self, R=10e9, L=1, C=1, R_L=10e12):
        self.R = R
        self.L = L
        self.C = C
        self.R_L = R_L
    
    @property
    def w_0(self):
        return 1/np.sqrt(self.L*self.C)
    
    @property
    def f_0(self):
        return self.w_0/(2*pi)
    
    @property
    def Q_0(self):
        return self.R/(self.w_0 * self.L)
    
    @property
    def Q_e(self):
        return self.R_L/(self.w_0 * self.L)
    
    @property
    def Q(self):
        return (1/self.Q_e + 1/self.Q_0)**-1

class RectangularCavity:
    
    def __init__(self, a, b, d, ep_r, mu_r, rho=0, tand=0):
        self.a = a
        self.b = b
        self.d = d
        self.ep_r = ep_r
        self.mu_r = mu_r
    
    def k_res(self, m, n, l):
        """
        Returns the resonant wavenumber of the TE/TM_{m,n,l} resonant mode.
        """
        
        return np.sqrt((m*pi/self.a)**2 + (n*pi/self.b)**2 + (l*pi/self.d)**2)
    
    def f_res(self, m, n, l):
        """
        Returns the resonant frequency of the TE/TM_{m,n,l} resonant mode.
        """
        
        return c_0/(2*pi*np.sqrt(self.mu_r * self.ep_r)) * self.k_res(m, n, l)
    
    def beta_res(self, m, n, l):
        w = 2*pi*self.f_res(m, n, l)
        k = w*np.sqrt(self.mu_r*mu_0*self.ep_r*ep_0)
        beta = (k**2 - (m*pi/self.a)**2 - (n*pi/self.b)**2)**0.5
        return beta
    
    def nontrivial_mode(self, m, n, l):
        return (l*pi/self.beta_res(m, n, l)) == self.d
    
    def lowest_modes(self, nmodes=5, k=5):
        mode_list = []
        for m, n, l in itertools.product(range(nmodes*k+1), range(nmodes*k+1), range(nmodes*k+1)):
            if self.nontrivial_mode(m, n, l):
                mode_list.append((m, n, l, self.f_res(m, n, l)))
        mode_array = np.array(mode_list, dtype=[('m', 'i'), ('n', 'i'), ('l', 'i'), ('f_res', 'f')])
        return np.sort(mode_array, order='f_res')[:nmodes]
    
    def mode_report(self, nmodes=5, k=5):
        sorted_array = self.lowest_modes(nmodes, k)
        mode_list = []
        for i in range(nmodes):
            m = sorted_array[i][0]
            n = sorted_array[i][1]
            l = sorted_array[i][2]
            fres = str(EngNumber(float(sorted_array[i][3])))
            mode_list.append([m, n, l, fres])
        table = tabulate(mode_list, ["m", "n", "l", "f_res (Hz)"])
        print(table)