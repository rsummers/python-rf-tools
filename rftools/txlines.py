#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 13:09:13 2019

@author: rsummers
"""

# from engineering_notation import EngNumber

from abc import ABC, abstractmethod

import numpy as np
import scipy
from scipy import constants, special
from engineering_notation import EngNumber
from tabulate import tabulate

import skrf as rf

e0 = constants.epsilon_0
u0 = constants.mu_0
pi = constants.pi
eta = np.sqrt(u0/e0)

class TXLine(ABC):
    @abstractmethod
    def __init__(self, frequency):
        self.frequency = frequency

    @property
    @abstractmethod
    def attenuation_constant(self):
        pass
    
    @property
    @abstractmethod
    def phase_constant(self):
        pass
    
    @property
    def propogation_constant(self):
        return self.attenuation_constant + 1j * self.phase_constant
    
    @property
    def wavelength(self):
        return 2*np.pi/self.phase_constant
    
    @property
    def phase_velocity(self):
        return 2*np.pi*self.frequency/self.propogation_constant



class CBCPW:
    def __init__(self, S, W, h, g=None, t=None, er=1):
        self.S = S
        self.W = W
        self.h = h
        
        if g:
            self.g = g
        else:
            self.g = g*1e9
        
        if t:
            self.t = t
        else:
            self.t = t*1e-9
        
        self.er = er
    
    @property
    def a(self):
        return self.S/2
    
    @property
    def b(self):
        return self.a + self.W
    
    def Z0(self, W=None):
        if W is None:
            W = self.W

        ep_effm = (self.er + 1)/2 + (self.er - 1)/2*np.sqrt(1 + 10*self.h/self.S)
        
        if self.S / self.h <= 1:
            Zm = eta/(2*pi*np.sqrt(ep_effm))*np.log(8*self.h/self.S + 0.25*self.S/self.h)
        else:
            Zm = eta/np.sqrt(ep_effm)*1.0/(self.S/self.h + 1.393 + 0.667*np.log(self.S/self.h + 1.444))
        
        k = self.S/(self.S + 2*W)
        kp = np.sqrt(1 - k**2)

        ep_effe = (self.er + 1)/2*(np.tan(0.775*np.log(self.h/W) + 1.75) + \
            k*W/self.h*(0.04 - 0.7*k + 0.01*(1 - 0.1*self.er)*(0.25 + k)))

        Zc = 30*pi/np.sqrt(ep_effe)*special.ellipk(kp)/special.ellipk(k)

        q = self.S/self.h * ((self.S+2*W)/self.S - 1) * \
            (3.6 - 2*np.exp(-(self.er + 1)/4))
        
        Z0 = 1/(5*q/(1 + 5*q)*1/Zm + 1/(1+q)*1/Zc)
        return Z0
    
    def synthesize_width(self, z0, el):
        imped = lambda w : (self.Z0(w) - z0)
        xin = 3e-3
        width = scipy.optimize.newton_krylov(imped, xin, f_tol=1e-12)

        return width

def synthesize_mline(mprops, z0, el, f0, frequency=None):
    nfreq = 101
    freq = rf.frequency.Frequency(0.9*f0, 1.1*f0, nfreq)
    i0 = int((nfreq - 1)/2)

    imped = lambda w : (rf.media.MLine(w=w, frequency=freq, **mprops).Z0_f[i0] - z0)
    xin = 3e-3
    width = scipy.optimize.newton_krylov(imped, xin, f_tol=1e-12)

    line = rf.media.MLine(w=float(width), frequency=freq, **mprops)
    ep_reff = np.real(line.ep_reff_f[i0])
    beta = 2*pi*f0*1e9 * np.sqrt(u0*e0*ep_reff)
    l = (el*pi/180)/beta

    print("An MLine with z0 =", str(z0), "ohms and electrical length of", str(el), "degrees")
    print("has w =", str(EngNumber(float(width))) + "m", "and L =", str(EngNumber(l)) + "m")

    if frequency is None:
        return line
    return rf.media.MLine(w=width, frequency=frequency, **mprops)

class RectangularWaveguide:

    def __init__(self, a, b, er, ur, tand = 0, conductivity=5.813e7, ur_conductor=1):
        self.a = a
        self.b = b
        self.er = er
        self.ur = ur
        self.tand = tand
        self.conductivity = conductivity
        self.ur_conductor = ur_conductor

    def valid_mode(self, mtype="TE", m=1, n=0):
        """
        Returns True if the mode passed to the function is a propogating
        mode in a rectangular waveguide, and returns False otherwise.
        """

        if mtype == "TE" and ((m > 0) or (n > 0)):
            return True
        elif (mtype == "TM") and (m > 0) and (n > 0):
            return True
        return False

    def k(self, f):
        """
        Returns the wavenumber in the waveguide at the given frequency.
        """

        return 2*np.pi*f*np.sqrt(self.ur*u0*self.er*e0)

    def kc(self, mtype="TE", m=1, n=0):
        """
        Returns the cutoff wavenumber of the given mode in the waveguide.
        """

        if self.valid_mode(mtype, m, n):
            return np.sqrt((m*np.pi/self.a)**2 + (n*np.pi/self.b)**2)
        return None

    def beta(self, f, mtype="TE", m=1, n=0):
        """
        Returns the propogation constant of the rectangular waveguide's
        given mode at the given frequency.
        """

        if self.valid_mode(mtype, m, n):
            return np.sqrt(self.k(f)**2 - self.kc(mtype, m, n)**2)
        return None

    def fc(self, mtype="TE", m=1, n=0):
        """
        Returns the cutoff frequency of the given mode.
        """

        if self.valid_mode(mtype, m, n):
            return (1/(2*np.pi*np.sqrt(self.er*e0*self.ur*u0)))*self.kc(mtype, m, n)
        return None

    def alphad(self, f, mtype="TE", m=1, n=0):
        """
        Returns the dielectric attenuation of the given mode in the waveguide
        in Np/m.
        """

        if self.valid_mode(mtype, m, n):
            return self.k(f)**2 * self.tand / (2*self.beta(f, mtype, m, n))
        return None

    def alphac10(self, f):
        """
        Returns the attenuation due to conductor loss for the TE_10 mode
        in Np/m.
        """
        w = 2*np.pi*f
        Rs = np.sqrt(w*self.ur_conductor*u0/(2*self.conductivity))
        eta = np.sqrt(self.ur*u0/(self.er*e0))
        term1 = Rs/(self.a**3 * self.b * self.beta(f, "TE", 1, 0) * self.k(f) * eta)
        term2 = 2*self.b*np.pi**2 + self.a**3 * self.k(f)**2
        return term1*term2

    def z(self, f, mtype="TE", m=1, n=0):
        """
        Returns the wave impedance of the given mode.
        """
        eta = np.sqrt(self.ur*u0/(self.er*e0))
        if not self.valid_mode(mtype, m, n):
            return None
        if mtype == "TE":
            return self.k(f)*eta/self.beta(f, mtype, m, n)
        elif mtype == "TM":
            return self.beta(f, mtype, m, n)*eta/self.k(f)
        return None

    def mode_report(self, nmodes=5, k=5):
        """
        Prints a table listing the n lowest propogating modes on the waveguide,
        checking all modes up to TE/TM_{nmodes*k, nmodes*k}
        """

        mode_list = []
        for i in range(nmodes*k + 1):
            for j in range(nmodes*k + 1):
                if (i != 0) or (j != 0):
                    cutoff = self.fc("TE", i, j)
                    if self.valid_mode("TM", i, j):
                        mtype = "TE, TM"
                    else:
                        mtype = "TE"
                    mode_list.append((mtype, i, j, cutoff))
        mode_array = np.array(mode_list, dtype=[('Mode', 'U6'), ('m', 'i4'), ('n', 'i4'), ('f_c', 'f')])
        sorted_array = np.sort(mode_array, order='f_c')[:nmodes]

        final_list = []
        for i in range(nmodes):
            mode = sorted_array[i][0]
            m = sorted_array[i][1]
            n = sorted_array[i][2]
            fc = str(EngNumber(float(sorted_array[i][3])))
            final_list.append([mode, m, n, fc])
        table = tabulate(final_list, ["Mode", "m", "n", "f_c (Hz)"])
        print(table)
