import rftools as rft

z0 = 50
el = 90
f0 = 2.45

mprops = {'h': 1.524e-3,
          't': 17.5e-6,
          'ep_r': 3.66,
          'mu_r': 1,
          'tand': 0.0031
       }

def test_synthesize_mline():
    w = rft.txlines.synthesize_mline(mprops, z0, el, f0).w
    assert 3.335e-3 <= w <= 3.345e-3
